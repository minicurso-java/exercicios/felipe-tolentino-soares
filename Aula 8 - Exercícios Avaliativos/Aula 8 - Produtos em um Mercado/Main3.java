/*
Crie uma classe Produto que represente um produto em um mercado.
Ela deve ter atributos como nome, preco e quantidade em estoque.
Implemente métodos para adicionar e remover unidades do estoque
e calcular o valor total em estoque.
*/

package aula8;

import java.util.Scanner;

import entidades.Produto;

public class Main3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		Produto prod1 = new Produto();
		int x=0;
		
		System.out.printf("Insira o nome do produto: ");
		prod1.nome = sc.nextLine();
		System.out.printf("Insira o preço do produto: ");
		prod1.preço = sc.nextDouble();
		System.out.printf("Insira a quantidade em estoque: ");
		prod1.quantidade = sc.nextInt();
		
		do {
			System.out.printf("--------------------------\n(0) Calcular o valor total em estoque\n(1) Adicionar quantidade de produtos ao estoque\n(2) Remover quantidade de produtos ao estoque\n(-1) Sair\n--------------------------\nEscolha: ");
			x = sc.nextInt();
			if(x==0) {
				prod1.total = prod1.calcTotal(prod1.preço);
				System.out.printf("O valor total em estoque é %.2f\n", prod1.total);
			} else if (x==1) {
				System.out.printf("Insira um número de produtos para adicionar ao estoque: ");
				int quant = sc.nextInt();
				boolean escolha = true;
				prod1.quantidade = prod1.calcQuant(quant, escolha);
				System.out.printf("--------------------------\nA quantidade de %s no valor de %.2f é %d\n", prod1.nome, prod1.preço, prod1.quantidade);
			} else if (x==2) {
				System.out.printf("Insira um número de produtos para remover do estoque: ");
				int quant = sc.nextInt();
				boolean escolha = false;
				prod1.quantidade = prod1.calcQuant(quant, escolha);
				System.out.printf("--------------------------\nA quantidade de %s no valor de %.2f é %d\n", prod1.nome, prod1.preço, prod1.quantidade);
			} else {
				System.out.printf("Saindo...");
			}
		} while(x!=-1);
		
		sc.close();
	}
}