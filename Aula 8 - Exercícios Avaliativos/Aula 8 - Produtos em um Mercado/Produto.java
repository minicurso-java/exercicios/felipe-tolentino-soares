package entidades;

public class Produto {
	public String nome;
	public double preço;
	public int quantidade;
	
	public int calcQuant(int valor, boolean opção) {
		if (opção==true) {
			 return (quantidade+valor);
		} else {
			return (quantidade-valor);
		}
	}
	
	public double total;
	public double calcTotal(double preço) {
		return (preço*quantidade);
	}
}
