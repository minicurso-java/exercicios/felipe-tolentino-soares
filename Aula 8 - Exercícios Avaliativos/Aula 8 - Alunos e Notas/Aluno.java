package entidades;

public class Aluno {
	public String nome;
	public int matricula;
	public double [] notas = new double [5];
	
	public double media;
	public double mediaNotas (double notas[]) {
		double soma = 0;
		for(int i=0 ; i<5 ; i++) {
			soma += notas[i];
		}
		return (soma/5);
	}
	
	public String Aprovação (double media) {
		if (media>6){
			return "Aprovado!";
		} else {
			return "Reprovado!";
		}
	}
}