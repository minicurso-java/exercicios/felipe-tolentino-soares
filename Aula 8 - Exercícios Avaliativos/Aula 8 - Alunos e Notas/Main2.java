package aula8;

import java.util.Scanner;

import entidades.Aluno;

public class Main2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		Aluno al1 = new Aluno();
		
		System.out.printf("Insira o nome do aluno: ");
		al1.nome = sc.nextLine();
		
		System.out.printf("Insira a matrícula do aluno: ");
		al1.matricula = sc.nextInt();
		
		System.out.printf("Insira as notas do aluno:\n");
		for(int i=0 ; i<5 ; i++) {
			System.out.printf("%dª nota: ",i+1);
			al1.notas[i] = sc.nextDouble();
		}
		al1.media = al1.mediaNotas(al1.notas);
		System.out.printf("O aluno %s de matrícula número %d teve média %.2f\n%s", al1.nome, al1.matricula, al1.media, al1.Aprovação(al1.media));
		
		sc.close();
	}
}