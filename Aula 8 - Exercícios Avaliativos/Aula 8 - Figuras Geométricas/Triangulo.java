package entidades;

public class Triangulo extends FiguraGeometrica {
	public double base;
	public double altura;
	public double lado1, lado2, lado3;
	
	public double calcularArea() {
		return ((base*altura)/2);
	}
	
	public double calcularPerimetro() {
		return (lado1+lado2+lado3);
	}
}
