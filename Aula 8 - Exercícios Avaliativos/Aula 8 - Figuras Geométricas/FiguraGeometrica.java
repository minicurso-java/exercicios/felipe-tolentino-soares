package entidades;

public abstract class FiguraGeometrica {
	public String nome;
	
	public abstract double calcularArea();
	public abstract double calcularPerimetro();
}
