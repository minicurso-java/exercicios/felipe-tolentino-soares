package aula8;

import java.util.Scanner;

import entidades.Triangulo;
import entidades.Circulo;
import entidades.Quadrado;

public class Main4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		Triangulo tri = new Triangulo();
		Circulo cir = new Circulo();
		Quadrado qua = new Quadrado();
		int x = 0, y = 0;
		
		System.out.printf("Insira o nome das suas formas\n");
		System.out.printf("Triangulo: ");
		tri.nome = sc.nextLine();
		System.out.printf("Circulo: ");
		cir.nome = sc.nextLine();
		System.out.printf("Quadrado: ");
		qua.nome = sc.nextLine();
		
		do {
			System.out.printf("--------------------------\nESOLHA QUAL FORMA QUER ALTERAR\n(0) Quadrado\n(1) Circulo\n(2) Triangulo\n(-1) Sair\n--------------------------\nEscolha: ");
			x = sc.nextInt();
			if(x==0) {
				do {
					System.out.printf("--------------------------\nESOLHA O QUE QUER CALCULAR\n(0) Area\n(1) Perimetro\n(-1) Sair\n--------------------------\nEscolha: ");
					y = sc.nextInt();
					if(y==0) {
						System.out.printf("Insira o valor do lado do quadrado '%s': ",qua.nome);
						qua.lado = sc.nextDouble();
						System.out.printf("O valor da área do quadrado '%s' é: %.2f\n",qua.nome, qua.calcularArea());
					} else if (y==1) {
						System.out.printf("Insira o valor do lado do quadrado '%s': ",qua.nome);
						qua.lado = sc.nextDouble();
						System.out.printf("O valor do perímetro do quadrado '%s' é: %.2f\n",qua.nome, qua.calcularPerimetro());
					} else {
						System.out.printf("Saindo...\n");
					}
				} while (y!=-1);
			} else if (x==1) {
				do {
					System.out.printf("--------------------------\nESOLHA O QUE QUER CALCULAR\n(0) Area\n(1) Perimetro\n(-1) Sair\n--------------------------\nEscolha: ");
					y = sc.nextInt();
					if(y==0) {
						System.out.printf("Insira o valor do raio do circulo '%s': ",cir.nome);
						cir.raio = sc.nextDouble();
						System.out.printf("O valor da área do circulo '%s' é: %.2f\n",cir.nome, cir.calcularArea());
					} else if (y==1) {
						System.out.printf("Insira o valor do raio do circulo '%s': ",cir.nome);
						cir.raio = sc.nextDouble();
						System.out.printf("O valor do perímetro do circulo '%s' é: %.2f\n",cir.nome, cir.calcularPerimetro());
					} else {
						System.out.printf("Saindo...\n");
					}
				} while (y!=-1);
			} else if (x==2) {
				do {
					System.out.printf("--------------------------\nESOLHA O QUE QUER CALCULAR\n(0) Area\n(1) Perimetro\n(-1) Sair\n--------------------------\nEscolha: ");
					y = sc.nextInt();
					if(y==0) {
						System.out.printf("Insira o valor da base do triangulo '%s': ",tri.nome);
						tri.base = sc.nextDouble();
						System.out.printf("Insira o valor da altura do triangulo '%s': ",tri.nome);
						tri.altura = sc.nextDouble();
						System.out.printf("O valor da área do triangulo '%s' é: %.2f\n",tri.nome, tri.calcularArea());
					} else if (y==1) {
						System.out.printf("Insira o valor do 1º lado do triangulo '%s': ",tri.nome);
						tri.lado1 = sc.nextDouble();
						System.out.printf("Insira o valor do 2º lado do triangulo '%s': ",tri.nome);
						tri.lado2 = sc.nextDouble();
						System.out.printf("Insira o valor do 3º lado do triangulo '%s': ",tri.nome);
						tri.lado3 = sc.nextDouble();
						System.out.printf("O valor do perímetro do triangulo '%s' é: %.2f\n",tri.nome, tri.calcularPerimetro());
					} else {
						System.out.printf("Saindo...\n");
					}
				} while (y!=-1);
			} else {
				System.out.printf("Saindo...\n");
			}
		} while (x!=-1);

		sc.close();
	}
}