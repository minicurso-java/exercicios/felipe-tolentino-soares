package entidades;

public class ContaBancaria {
	public int numero;
	public double saldo;
	public String titular;
	
	public double calcSaldo(double valor, boolean opção) {
		if (opção==true) {
			 return (saldo-valor);
		} else {
			return (saldo+valor);
		}
	}
	
	public double verificarSaldo(double saldo) {
		return saldo;
	}
}