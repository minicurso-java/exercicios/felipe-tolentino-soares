/*
Crie uma classe ContaBancaria que represente uma conta bancária
básica. Ela deve ter atributos como numero, saldo e titular. 
Implemente métodos para depositar, sacar e 
verificar o saldo da conta.
*/

package aula8;

import java.util.Scanner;

import entidades.ContaBancaria;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		ContaBancaria conta = new ContaBancaria();
		int x=0;
		
		System.out.printf("Insira o número da sua conta: ");
		conta.numero = sc.nextInt();
		System.out.printf("Insira o saldo da sua conta: ");
		conta.saldo = sc.nextDouble();
		System.out.printf("Insira o titular da sua conta: ");
		sc.nextLine();
		conta.titular = sc.nextLine();
		
		do {
			System.out.printf("--------------------------\n(0) Conferir saldo\n(1) Sacar dinheiro\n(2) Depositar dinheiro\n(-1) Sair\n--------------------------\nEscolha: ");
			x = sc.nextInt();
			if(x==0) {
				System.out.printf("Conta número %d de titular %s possui saldo de %.2f\n", conta.numero, conta.titular, conta.verificarSaldo(conta.saldo));
			} else if (x==1) {
				System.out.printf("Insira um valor para sacar: ");
				double dinheiro = sc.nextDouble();
				boolean escolha = true;
				conta.saldo= conta.calcSaldo(dinheiro, escolha);
			} else if (x==2) {
				System.out.printf("Insira um valor para depositar: ");
				double dinheiro = sc.nextDouble();
				boolean escolha = false;
				conta.saldo= conta.calcSaldo(dinheiro, escolha);
			} else {
				System.out.printf("Saindo...");
			}
		
		} while(x!=-1);
		
		sc.close();
	}
}